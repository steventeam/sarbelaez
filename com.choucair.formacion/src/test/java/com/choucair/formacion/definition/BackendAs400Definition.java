package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.BackendAs400Steps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class BackendAs400Definition {
	
	@Steps
	BackendAs400Steps backendAs400Steps;

	@Given("^Consultar CNAME$")
	public void consultar_CNAME(DataTable dtDatosPrueba) throws Throwable {
	    List<List<String>> data = dtDatosPrueba.raw();
	    backendAs400Steps.Consultar_CNAME(data);
		
	}
}
