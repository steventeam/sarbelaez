package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.ColorlibFormValidationSteps;
import com.choucair.formacion.steps.PopupValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.*;
import net.thucydides.core.annotations.Steps;

public class PopupValidationDefinition {

	@Steps
	PopupValidationSteps popupValidationSteps;
	
	@Steps
	ColorlibFormValidationSteps colorlibFormValidationSteps;
	
	
	@Given("^Autentico en colorlib con usuario \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void autentico_en_colorlib_con_usuario_y_clave(String Usuario, String Pass) {
	    popupValidationSteps.loginColorlib(Usuario, Pass);
	}

	@Given("^Ingreso a la funcionalidad Forms Validation$")
	public void ingreso_a_la_funcionalidad_Forms_Validation() {
	   popupValidationSteps.ingreso_a_la_funcionalidad_Forms_Validation();
	}

	@When("^Diligencio Formulario Popup Validation$")
	public void diligencio_Formulario_Popup_Validation(DataTable dtDatosForms) {
	    List<List<String>> data = dtDatosForms.raw();
	    
	    //String [] DataPrueba = new String [data.size()];
	    
	    for (int i = 1; i < data.size(); i++){
	    	colorlibFormValidationSteps.diligencio_Formulario_Popup_Validation(data, i);
	    	
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
	    }
	}

	@Then("^Verifico ingreso exitoso$")
	public void verifico_ingreso_exitoso() {
	   colorlibFormValidationSteps.verificar_ingreso_exitoso();
	}	
	
	@Then("^Verifico que se presente globo informativo$")
	public void verifico_que_se_presente_globo_informativo() {
		colorlibFormValidationSteps.verificar_ingreso_con_errores();
	}
}
