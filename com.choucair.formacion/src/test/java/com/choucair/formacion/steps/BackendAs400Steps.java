package com.choucair.formacion.steps;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.choucair.formacion.pageobjects.BackendAs400Page;

import net.thucydides.core.annotations.Step;

public class BackendAs400Steps {

	BackendAs400Page backendAs400Page;
	
	@Step
	public void Consultar_CNAME(List<List<String>> data) throws SQLException {
		//Crear query
		String strDocumento = data.get(0).get(0);
		String query = backendAs400Page.Armar_Query_Consulta_CNAME(strDocumento);
		//Ejecuta consulta Sql
		ResultSet rs = backendAs400Page.Ejecutar_Query(query);
		//Verificar resultados
		backendAs400Page.Verificar_Consulta_CNAME(rs, data);
	}
}
