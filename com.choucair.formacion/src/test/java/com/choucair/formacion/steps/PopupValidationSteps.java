package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.ColorlibLoginPage;
import com.choucair.formacion.pageobjects.ColorlibMenuPage;

import net.thucydides.core.annotations.Step;

public class PopupValidationSteps {

	ColorlibLoginPage colorlibLoginPage;
	ColorlibMenuPage colorlibMenuPage;
	
	@Step
	public void loginColorlib(String strUsuario, String strPass) {
		
//		a. Abrir navegador con la url de prueba
		colorlibLoginPage.open();
//		b. Ingresar usuario demo
//		c. Ingresar password demo
//		d. Click en botón Sign in
		colorlibLoginPage.IngresarDatos(strUsuario, strPass);
//		e. Verificar la Autenticación (label en home)
		colorlibLoginPage.VerificarHome();
		
	}
	
	@Step
	public void ingreso_a_la_funcionalidad_Forms_Validation() {
		
//		Ingresar a Funcionalidad Popup Validation
//		a. Clic en Menu “Forms”
//		b. Clic en submenú “Form Validation”
//		c. Verificación : se presenta pantalla de la funcionalidad con título Popup Validation
		colorlibMenuPage.verificarForms();
		
	}
}
