package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://colorlib.com/polygon/metis/login.html")
public class ColorlibFormValidationPage extends PageObject {

	//Required
	@FindBy(xpath = "//*[@id='req']")
	public WebElementFacade txtRequired;
	
	//Select
	@FindBy(xpath = "//*[@id='sport']")
	public WebElementFacade cmbSport1;
	
	//Select
	@FindBy(xpath = "//*[@id='sport2']")
	public WebElementFacade cmbSport2;
	
	//Url
	@FindBy(xpath = "//*[@id='url1']")
	public WebElementFacade txtUrl;
	
	//email1
	@FindBy(id="email1")
	public WebElementFacade txtEmail;
	
	//Password
	@FindBy(id="pass1")
	public WebElementFacade txtPass1;
	
	//Confirm Password
	@FindBy(id="pass2")
	public WebElementFacade txtPass2;
	
	//Minimum field size (6)
	@FindBy(id="minsize1")
	public WebElementFacade txtMinsize1;
	
	//Maximum field size, optional
	@FindBy(id="maxsize1")
	public WebElementFacade txtMaxsize1;
	
	//Number
	@FindBy(id="number2")
	public WebElementFacade txtNumber2;
	
	//IP
	@FindBy(id="ip")
	public WebElementFacade txtIp;
	
	//date3
	@FindBy(id="date3")
	public WebElementFacade txtDate3;
	
	//Date Earlier
	@FindBy(id="past")
	public WebElementFacade txtPast;
	
	//Validate
	@FindBy(xpath = "//*[@id='popup-validation']/div[14]/input")
	public WebElementFacade btnValidate;
	
	//Globo informativo
	@FindBy(xpath = "(//DIV[@class='formErrorContent'])[1]")
	public WebElementFacade globoInformativo;
	
	public void Validate() {
		btnValidate.click();
	}
	
	public void Required(String datoPrueba) {
		txtRequired.click();
		txtRequired.clear();
		txtRequired.sendKeys(datoPrueba);
	}
	
	public void Select_Sport(String datoPrueba) {
		cmbSport1.click();
		cmbSport1.selectByVisibleText(datoPrueba);
	}
	
	public void Multiple_Select(String datoPrueba) {
		cmbSport2.click();
		cmbSport2.selectByVisibleText(datoPrueba);
	}
	
	public void Url(String datoPrueba) {
		txtUrl.click();
		txtUrl.clear();
		txtUrl.sendKeys(datoPrueba);
	}
	
	public void Email(String datoPrueba) {
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(datoPrueba);
	}
	
	public void Pass1(String datoPrueba) {
		txtPass1.click();
		txtPass1.clear();
		txtPass1.sendKeys(datoPrueba);
	}
	
	public void Confirm_Password(String datoPrueba) {
		txtPass2.click();
		txtPass2.clear();
		txtPass2.sendKeys(datoPrueba);
	}

	public void Date(String datoPrueba) {
		txtDate3.click();
		txtDate3.clear();
		txtDate3.sendKeys(datoPrueba);
	}

	public void Date_Earlier(String datoPrueba) {
		txtPast.click();
		txtPast.clear();
		txtPast.sendKeys(datoPrueba);
	}

	public void MinSize(String datoPrueba) {
		txtMinsize1.click();
		txtMinsize1.clear();
		txtMinsize1.sendKeys(datoPrueba);
	}
	
	public void MaxSize(String datoPrueba) {
		txtMaxsize1.click();
		txtMaxsize1.clear();
		txtMaxsize1.sendKeys(datoPrueba);
	}
	
	public void Number(String datoPrueba) {
		txtNumber2.click();
		txtNumber2.clear();
		txtNumber2.sendKeys(datoPrueba);
	}
	
	public void Ip(String datoPrueba) {
		txtIp.click();
		txtIp.clear();
		txtIp.sendKeys(datoPrueba);
	}
	
	//Validar globo
	public void formSinErrores() {
		
		assertThat(globoInformativo.isCurrentlyVisible(), is(false));
	}
	
	public void formConErrores() {
		
		assertThat(globoInformativo.isCurrentlyVisible(), is(true));
	}
	
}
