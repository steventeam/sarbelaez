package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://colorlib.com/polygon/metis/login.html")
public class ColorlibLoginPage extends PageObject {

	//usuario
	@FindBy(xpath = "//*[@id='login']/form/input[1]")
	public WebElementFacade txtUsuario;
	
	//password
	@FindBy(xpath = "//*[@id='login']/form/input[2]")
	public WebElementFacade txtPass;
	
	//Botón
	@FindBy(xpath = "//*[@id='login']/form/button")
	public WebElementFacade btnSignIn;
	
	//Botón
	@FindBy(xpath = "//*[@id='bootstrap-admin-template']")
	public WebElementFacade lblHomePpal;

	
	public void IngresarDatos(String strUsuario, String strPass) {
		txtUsuario.sendKeys(strUsuario);
		txtPass.sendKeys(strPass);
		btnSignIn.click();
	}

	public void VerificarHome() {
		
		String labelHome = "Bootstrap-Admin-Template";
		String strMensaje =lblHomePpal.getText();
		assertThat(strMensaje, containsString(labelHome));
	}
	
	
}
