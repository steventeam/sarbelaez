package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class ColorlibMenuPage extends PageObject {

	//Forms
	@FindBy(xpath = "//*[@id='menu']/li[6]/a")
	public WebElementFacade menuForms;
		
	//Forms validation
	@FindBy(xpath = "//*[@id='menu']/li[6]/ul/li[2]/a")
	public WebElementFacade menuFormsValidation;
	
	//Capturar label
	@FindBy(xpath = "//*[@id='content']/div/div/div[1]/div/div/header/h5")
	public WebElementFacade lblPopup;

	//comparar label
	public void verificarForms() {
		
		menuForms.click();
		menuFormsValidation.click();
		
		String labelPopup = "Popup Validation";
		String strMensaje = lblPopup.getText();
		assertThat(strMensaje, containsString(labelPopup));
	}
	
	
}
