package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.choucair.as400.utilities.Sql_Execute;

import net.serenitybdd.core.pages.PageObject;

public class BackendAs400Page extends PageObject{
	
	public String Armar_Query_Consulta_CNAME(String strDocumento) {
		String strQuery = "SELECT CNNOSS, CNCDTI, CNNAME, CNCDCC FROM DDSLIBRACL.TMPCNAME2 WHERE CNNOSS = '<documento>'";
		strQuery = strQuery.replace("<documento>", strDocumento);
		return strQuery;
	}
	
	public ResultSet Ejecutar_Query(String Query) throws SQLException {
		Sql_Execute DAO = new Sql_Execute();
		ResultSet rs = DAO.sql_Execute(Query);
		return rs;
		
	}
	
	public void Verificar_Consulta_CNAME(ResultSet rs, List<List<String>> data) throws SQLException {
		while (rs.next()) {
			String Documento_Recibido = rs.getString(1);
			String Documento_Esperado = data.get(0).get(0);
			assertThat(Documento_Recibido, equalTo(Documento_Esperado));
			String TipoDoc_Recibido = rs.getString(2);
			String TipoDoc_Esperado = data.get(0).get(1);
			assertThat(TipoDoc_Recibido, equalTo(TipoDoc_Esperado));
			String Nombre_Recibido = rs.getString(3);
			String Nombre_Esperado = data.get(0).get(2);
			assertThat(Nombre_Recibido.trim(), equalTo(Nombre_Esperado.trim()));
			String Tercero_Recibido = rs.getString(4);
			String Tercero_Esperado = data.get(0).get(3);
			assertThat(Tercero_Recibido.trim(), equalTo(Tercero_Esperado.trim()));
		}
	}

}
