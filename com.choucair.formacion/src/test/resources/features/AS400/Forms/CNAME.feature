#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Consultar CNAME
  Verificar el acceso a los datos del CNAME.
	Criterios de Aceptación: 
	•	Verificar diligenciamiento exitoso. 
	•	Verificar mensaje de validación para cada campo

  @CasoFeliz
  Scenario Outline: Consultar tabla de clientes CNAME y verificar resultados
    Given Consultar CNAME
    
    | <Documento>  | <Tipo doc> | <Nombre> | <Terceros> |
    
#    When I check for the <value> in step
#    Then I verify the <status> in step
#
    Examples: 
      | Documento  		 | Tipo doc | Nombre 		  |Terceros |
      | 000001008176901  |   1      | CORE HIPO		  | 1		|
      | 000001000378801  |   1      | CORE HIPO		  | 1		|

