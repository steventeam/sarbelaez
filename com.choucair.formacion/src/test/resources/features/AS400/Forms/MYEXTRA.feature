#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Abrir MyExtra
  Verificar que se abra MyExtra.
	Criterios de Aceptación: 
	•	Verificar que si se abra MyExtra

  @MYEXTRA
  Scenario: Trabajar con el objeto MyExtra
    Given Abrir MyExtra "D:\\Mis Documentos\\Attachmate\\EXTRA!\\sessions\\CALIDAD.EDP"
    When Autenticar en MyExtra 
    	And Ingresar a AIP
#    Then I verify the <status> in step


