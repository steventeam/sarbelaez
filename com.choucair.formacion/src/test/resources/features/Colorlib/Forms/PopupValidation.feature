#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Formulario Popup Validation
  Verificar el diligenciamiento de la pantalla “Popup Validation”.
	Criterios de Aceptación: 
	•	Verificar diligenciamiento exitoso. 
	•	Verificar mensaje de validación para cada campo


  @CasoExitoso
  Scenario: Diligenciamiento exitoso del formulario
    Given Autentico en colorlib con usuario "demo" y clave "demo"
    	And Ingreso a la funcionalidad Forms Validation
    When Diligencio Formulario Popup Validation

| Required | Select | MultipleS1 | MultipleS2 | Url 						   | E-mail 			| Password | Confirm Password | Minimum field size | Maximum field size | Number  | IP 			| Date 		 | Date Earlier |
| Valor1   | Tennis | Tennis     | Golf	      | http://www.choucairtesting.com | prueba@demo.com	| miclave  | miclave	      | 123456			   | 123456 			| 2000.99 | 201.234.1.5 | 2017-10-09 | 2012-09-12   |
    
    Then Verifico ingreso exitoso

  @CasoAlterno
  Scenario: Diligenciamiento con errores del formulario.
  			Se presenta globo informativo cuando no se diligencia correctamente.
    Given Autentico en colorlib con usuario "demo" y clave "demo"
    	And Ingreso a la funcionalidad Forms Validation
    When Diligencio Formulario Popup Validation

| Required | Select | MultipleS1 | MultipleS2 | Url 						 	| E-mail 			| Password | Confirm Password | Minimum field size | Maximum field size | Number  | IP 			| Date 		 | Date Earlier |
| 		   | Tennis | Tennis     | Golf	      | http://www.choucairtesting.com 	| prueba@demo.com	| miclave  | miclave	      | 123456			   | 123456 			| 2000.99 | 201.234.1.5 | 2017-10-09 | 2012-09-12   |
|Valor1	   | Tennis | Tennis     | Golf	      | www.choucairtesting.com 		| prueba@demo.com	| miclave  | miclave	      | 123456			   | 123456 			| 2000.99 | 201.234.1.5 | 2017-10-09 | 2012-09-12   |
    
    Then Verifico que se presente globo informativo

#  @tag2
#  Scenario Outline: Title of your scenario outline
#    Given I want to write a step with <name>
#    When I check for the <value> in step
#    Then I verify the <status> in step
#
#    Examples: 
#      | name  | value | status  |
#      | name1 |     5 | success |
#      | name2 |     7 | Fail    |
